
TITLE: 
Gerko - Product Landing Page Template with Bootstrap 4

Link Demo: http://banner.nanothemes.co/gerko/

AUTHOR:
DESIGNED & DEVELOPED by nanothemes.co

Website: http://nanothemes.co/

CREDITS:

Bootstrap
http://getbootstrap.com/

jQuery
http://jquery.com/

Google Fonts
https://www.google.com/fonts/

Font Awesome
http://fontawesome.io

animate.css
http://daneden.me/animate

Owl Carousel
http://www.owlcarousel.owlgraphic.com/
